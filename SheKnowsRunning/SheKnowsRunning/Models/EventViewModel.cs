﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SheKnowsRunning.Models
{
    public class EventViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Register { get; set; }
        public string Time { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
        public string CourseDescription { get; set; }
        public string Zip { get; set; }
        public string ContactPhone { get; set; }
        public string Location { get; set; }
        public string RegistraionPageLink { get; set; }
        public string WebsiteLink { get; set; }
        public string ResultLink { get; set; }
        public string CourseMapLink { get; set; }
        public string FacebookLink { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLink { get; set; }
        public bool AgeGroupsAndAwards { get; set; }
        public string EventOneType { get; set; }
        public string EventOneTime { get; set; }
        public string EventOneFee { get; set; }
        public string EventTwoType { get; set; }
        public string EventTwoTime { get; set; }
        public string EventTwoFee { get; set; }
        public string EventThreeType { get; set; }
        public string EventThreeTime { get; set; }
        public string EventThreeFee { get; set; }
        public string EventFourType { get; set; }
        public string EventFourTime { get; set; }
        public string EventFourFee { get; set; }
        public string EventName { get; set; }
    }
}