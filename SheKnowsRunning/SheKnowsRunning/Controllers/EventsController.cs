﻿using SheKnowsRunning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SheKnowsRunning.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventsController : ApiController
    {
        // GET api/events
        public IEnumerable<string> Get()
        {
            return new string[] { "event1", "event2" };
        }

        // GET api/events
        public bool Create(EventViewModel eventModel)
        {
            return true;
        }
    }
}
